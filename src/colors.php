<?php
// Color system
$colorBrandGrey = 'rgb(51, 62, 72)';
$colorBrandGrey100= $colorBrandGrey;
$colorBrandGrey80 = 'rgb(92, 101, 109)';
$colorBrandGrey60 = 'rgb(132, 139, 145)';
$colorBrandGrey33 = 'rgb(188, 191, 194)';
$colorBrandGrey10 = 'rgb(235, 236, 237)';

$colorBrandPetrol = 'rgb(18, 99, 119)';
$colorBrandBlue = 'rgb(37, 117, 252)';
$colorBrandPurple = 'rgb(106, 17, 203)';
$colorBrandLightblue = 'rgb(0, 236, 188)';
$colorBrandYellow = 'rgb(255, 239, 58)';
$colorBrandNeon = 'rgb(216, 255, 0)';
$colorBrandMistblue = 'rgb(109, 148, 255)';
$colorBrandMint = 'rgb(130, 255, 136)';
$colorBrandOrange = 'rgb(255, 151, 0)';
$colorBrandGreen = 'rgb(0, 189, 107)';
$colorBrandRed = 'rgb(255, 8, 68)';
$colorBrandViolette = 'rgb(138, 51, 126)';
$colorBrandViolet = $colorBrandViolette;
$colorBrandFrog = 'rgb(0, 255, 0)';

$colorBlack = 'rgb(0, 0, 0)';
$colorWhite = 'rgb(255, 255, 255)';

$brandColors = [
    'blue' => $colorBrandBlue,
    'frog' => $colorBrandFrog,
    'green' => $colorBrandGreen,
    'grey'=> $colorBrandGrey,
    'lightblue' => $colorBrandLightblue,
    'mint' => $colorBrandMint,
    'mistblue' => $colorBrandMistblue,
    'neon' => $colorBrandNeon,
    'orange' => $colorBrandOrange,
    'petrol' => $colorBrandPetrol,
    'purple' => $colorBrandPurple,
    'red' => $colorBrandRed,
    'violet' => $colorBrandViolet,
    'yellow' => $colorBrandYellow,
    'white' => $colorWhite,
];
